/*
JCGE: Preparamos el template para las tablas

CREATE TABLE tabla (campo1 INTEGER, campo2 TEXT, campo3 DATE);
*/


CREATE TABLE bitacora   (idbitacora INTEGER,
                         bit_descrip TEXT,
                         bit_idusuario TEXT,
                         bit_fecha TIMESTAMP);

CREATE TABLE usuarios   (idusuario TEXT,
                         usu_nom TEXT,
                         usu_pass TEXT);

CREATE TABLE directorio (iddir INTEGER,
                         dir_nom_compl TEXT,
                         dir_fnac DATE,
                         dir_correo TEXT);

CREATE TABLE inventario (idinv INTEGER,
                         inv_nombre TEXT,
                         inv_modelo TEXT,
                         inv_descripcion TEXT,
                         inv_tipo INTEGER,
                         inv_imagen TEXT);

CREATE TABLE prestamos  (idprestamo INTEGER,
						 iddir INTEGER,
                         idinv INTEGER,
                         fecha_hora_salida TIMESTAMP,
                         fecha_hora_entrada TIMESTAMP,
                         idusuario TEXT);


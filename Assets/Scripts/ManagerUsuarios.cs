﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class ManagerUsuarios : MonoBehaviour
{
    public GameObject CUsuarios, CAdministracion, CNuevoUsuario, CEditarUsuario, CBorrarUsuario;

    [Header("Nuevo Usuario")]
    public GameObject[] inputsNuevoUsuario;

    [Header("Editar Usuario")]
    public GameObject[] inputsEditarUsuario;

    [Header("Borrar Usuario")]
    public GameObject inptUsuarioBorrar;


    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    //SeccionNuevoUsuario
    public void GuardarNuevoUsuario()
    {
        string usuario     = inputsNuevoUsuario[0].GetComponent<TMP_InputField>().text; //Usuario
        string nombre      = inputsNuevoUsuario[1].GetComponent<TMP_InputField>().text; //Nombre
        string cel         = inputsNuevoUsuario[2].GetComponent<TMP_InputField>().text; //Celular
        string correo      = inputsNuevoUsuario[3].GetComponent<TMP_InputField>().text; //Correo
        string contrasena1 = inputsNuevoUsuario[4].GetComponent<TMP_InputField>().text; //Contrasena1
        string contrasena2 = inputsNuevoUsuario[5].GetComponent<TMP_InputField>().text; //Contrasena2

        //Aplicamos las de ley
        if (usuario.Trim() == "")
        {
            Utilerias.ShowPopUp("Usuario: Favor de completar el campo");
            return;
        }

        if (usuario.Trim().Contains(" "))
        {
            Utilerias.ShowPopUp("Usuario: Favor de colocar un usuario sin espacios");
            return;
        }

        if (nombre.Trim() == "")
        {
            Utilerias.ShowPopUp("Nombre Completo: Favor de completar el campo");
            return;
        }

        if (contrasena1.Trim() == "")
        {
            Utilerias.ShowPopUp("Contraseña: Favor de completar el campo");
            return;
        }

        if (!object.Equals(contrasena1, contrasena2))
        {
            Utilerias.ShowPopUp("Contraseña: Las contraseñas tienen que coincidir");
            return;
        }

        Usuario usu = new Usuario(usuario, nombre, contrasena1, cel, correo);

        //Ya existe dentro de la base de datos???????
        if (usu.Existe(false))
        {
            Utilerias.ShowPopUp("Ya existe el usuario, favor de dar de alta uno diferente");
            return;
        }

        //Lo metemos alv
        usu.SqlInsert();
        Utilerias.ShowPopUp("Usuario agregado correctamente: "+usu.usu_nom);

        //Limpiamos estas weas
        foreach (GameObject go in inputsNuevoUsuario)
        {
            go.GetComponent<TMP_InputField>().text = "";
        }
        ManagerLogin.log_insert("Usuario agregado correctamente: "+usu.usu_nom);
    }

    public void RellenaEditarUsuario()
    {
        string usuario = inputsEditarUsuario[0].GetComponent<TMP_InputField>().text; //Usuario
        if (object.Equals(usuario,""))
        {
            return;
        }
        Usuario usu = new Usuario(usuario);
        if (usu.Existe(true))
        {
            inputsEditarUsuario[1].GetComponent<TMP_InputField>().text = usu.usu_nom; //Nombre
            inputsEditarUsuario[2].GetComponent<TMP_InputField>().text = usu.usu_celular; //Celular
            inputsEditarUsuario[3].GetComponent<TMP_InputField>().text = usu.usu_correo; //Correo
        }
        else
        {
            Utilerias.ShowPopUp("No existe el usuario");
            return;
        }
    }

    public void GuardarEditarUsuario()
    {
        string usuario     = inputsEditarUsuario[0].GetComponent<TMP_InputField>().text; //Usuario
        string nombre      = inputsEditarUsuario[1].GetComponent<TMP_InputField>().text; //Nombre
        string cel         = inputsEditarUsuario[2].GetComponent<TMP_InputField>().text; //Celular
        string correo      = inputsEditarUsuario[3].GetComponent<TMP_InputField>().text; //Correo
        string contrasena1 = inputsEditarUsuario[4].GetComponent<TMP_InputField>().text; //Contrasena1
        string contrasena2 = inputsEditarUsuario[5].GetComponent<TMP_InputField>().text; //Contrasena2

        //Aplicamos las de ley
        if (nombre.Trim() == "")
        {
            Utilerias.ShowPopUp("Nombre Completo: Favor de completar el campo");
            return;
        }

        if (!object.Equals(contrasena1, contrasena2))
        {
            Utilerias.ShowPopUp("Contraseña: Las contraseñas tienen que coincidir");
            return;
        }

        Usuario usu = new Usuario(usuario, nombre, contrasena1, cel, correo);

        //Como estamos editando, si no existe pues que pata
        if (!usu.Existe(false))
        {
            Utilerias.ShowPopUp("No existe el usuario");
            return;
        }

        //Lo metemos alv
        usu.SqlUpdate();
        Utilerias.ShowPopUp("Usuario editado correctamente: " + usu.usu_nom);

        //Limpiamos estas weas
        foreach (GameObject go in inputsEditarUsuario)
        {
            go.GetComponent<TMP_InputField>().text = "";
        }

        ManagerLogin.log_insert("Usuario editado correctamente: " + usu.usu_nom);
    }

    public void BorrarUsuario()
    {
        string usuario = inptUsuarioBorrar.GetComponent<TMP_InputField>().text; //Usuario
        Usuario usu = new Usuario(usuario);

        if (!usu.Existe(false))
        {
            Utilerias.ShowPopUp("No existe el usuario");
            return;
        }

        /*
        if (es el usuario en la sesion)
        {
        }
        */

        //Lo borramos alv
        usu.SqlDelete();

        Utilerias.ShowPopUp("Usuario borrado correctamente");
        inptUsuarioBorrar.GetComponent<TMP_InputField>().text = "";

        ManagerLogin.log_insert("Usuario borrado correctamente" + usu.usu_nom);
        return;
    }

    public void CancelarNuevoUsuario()
    {
        foreach (GameObject go in inputsNuevoUsuario)
        {
            go.GetComponent<TMP_InputField>().text = "";
        }
    }

    public void CancelarEditarUsuario()
    {
        foreach (GameObject go in inputsEditarUsuario)
        {
            go.GetComponent<TMP_InputField>().text = "";
        }
    }

    public void CancelarBorrarUsuario()
    {
        inptUsuarioBorrar.GetComponent<TMP_InputField>().text = "";
    }

    public void Regresar()
    {
        CUsuarios.SetActive(false);
        CAdministracion.SetActive(true);
    }

    public void GUINuevo()
    {
        CNuevoUsuario.SetActive(true);
        CEditarUsuario.SetActive(false);
        CBorrarUsuario.SetActive(false);
    }

    public void GUIEditar()
    {
        CNuevoUsuario.SetActive(false);
        CEditarUsuario.SetActive(true);
        CBorrarUsuario.SetActive(false);
    }

    public void GUIBorrar()
    {
        CNuevoUsuario.SetActive(false);
        CEditarUsuario.SetActive(false);
        CBorrarUsuario.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Crosstales.FB;

public class FileManager : MonoBehaviour
{
    string path;
    public GameObject image;
    Texture2D image2D;

    public void OpenExplorer()
    {
        path = FileBrowser.OpenSingleFile("Seleccionar una imagen", "", "png");
        GetImage();
    }

    void GetImage()
    {
        if (path != null)
        {
            UpdateImage();
        }
    }

    void UpdateImage()
    {
        WWW www = new WWW("file:///" + path);
        image.GetComponent<RawImage>().texture = www.texture;
        //image2D = (www.texture as Texture2D);
        //string b64 = System.Convert.ToBase64String(image2D.EncodeToPNG());
        //Debug.Log(b64);
    }
}

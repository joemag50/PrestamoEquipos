﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Data;

public class ManagerRetorno : MonoBehaviour
{
    public GameObject CRetorno, CPrincipal;

    [Header("Matricula")]
    public GameObject Matricula;

    [Header("Tablero Pedorro")]
    public GameObject tablapedorra;
    private IDataReader reader;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Retornar()
    {
        string matricula = Matricula.GetComponent<TMP_InputField>().text;
        Alumno a = new Alumno(matricula);

        if (!a.Existe())
        {
            Utilerias.ShowPopUp("No existe el Alumno");
            return;
        }

        Articulo arti = new Articulo(tablapedorra.GetComponent<RellenarLista>().getArticuloIdInventario());
        if (!arti.Existe())
        {
            Utilerias.ShowPopUp("No existe el articulo, favor de seleccionarlo");
            return;
        }

        Prestamo p = new Prestamo(tablapedorra.GetComponent<RellenarLista>().getIdPrestamo());

        p.sqlUpdate();

        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        Utilerias.ShowPopUp("Articulo retornado exitosamente");

        ManagerLogin.log_insert("Articulo retornado exitosamente "+arti.nombre+" "+arti.modelo);

        return;
    }

    public void ValidarMatricula()
    {
        string matricula = Matricula.GetComponent<TMP_InputField>().text;
        tablapedorra.GetComponent<RellenarLista>().setWhere(matricula);

        int cuantos = 0;
        reader = BaseDatos.SqlCommand(string.Format(" SELECT idprestamo, inv_nombre, inv_modelo FROM prestamos LEFT JOIN inventario USING (idinv) " +
                                            " WHERE dir_matricula = '{0}' AND (fecha_hora_entrada = '')", matricula));
        while (reader.Read())
        {
            cuantos++;
        }

        if (cuantos == 0)
        {
            Utilerias.ShowPopUp("No existe ningun prestamo pendiente");
            return;
        }
    }
    public void Regresar()
    {
        Matricula.GetComponent<TMP_InputField>().text = "";
        CRetorno.SetActive(false);
        CPrincipal.SetActive(true);
    }
}

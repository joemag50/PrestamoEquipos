﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RellenarLista : MonoBehaviour
{
    public GameObject prefabElementoLista;
    public List<GameObject> Elementos;
    public List<Articulo> articulos;
    public GameObject miImagen;
    public int valor; //Que ID tiene el elemento que agarro este puto
    public List<int> idprestamo; //El ID que tiene este puñetas
    public int index;
    public string WHERE;
    public bool BuscarPorMatricula; //Con esto solo van a ponerlo en true y solo agregamos en el WHERE cual matricula
    public bool BusquedaSinRetorno;

    // Use this for initialization
    void Start ()
    {
        RellenarListaDesdeQuery();
    }

    public void RellenarListaDesdeQuery()
    {
        Elementos = new List<GameObject>();
        articulos = new List<Articulo>();
        idprestamo = new List<int>();
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        int entero = 0;
        IDataReader reader;
        if (WHERE.Length > 0 && !BusquedaSinRetorno)
        {
            if (BuscarPorMatricula)
            {
                reader = BaseDatos.SqlCommand(string.Format(" SELECT idprestamo, inv_nombre, inv_modelo FROM prestamos LEFT JOIN inventario USING (idinv) " +
                                                            " WHERE dir_matricula = '{0}' AND (fecha_hora_entrada = '')", WHERE));
            }
            else
            {
                reader = BaseDatos.SqlCommand("SELECT * FROM inventario " + WHERE);
            }
        }
        else if (WHERE.Length > 0 && BusquedaSinRetorno)
        {
            reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM inventario {0} AND idinv NOT IN (SELECT idinv FROM prestamos WHERE fecha_hora_salida != '')", WHERE));
        }
        else if (BusquedaSinRetorno)
        {
            reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM inventario WHERE idinv NOT IN (SELECT idinv FROM prestamos WHERE fecha_hora_salida != '') "));
        }
        else
        {
            reader = BaseDatos.SqlCommand("SELECT * FROM inventario");
        }

        while (reader.Read())
        {
            string nombre = reader.GetString(1);
            string modelo = reader.GetString(2);
            GameObject e = Instantiate(prefabElementoLista, this.transform);

            string texto;
            if (BuscarPorMatricula)
            {
                idprestamo.Add(reader.GetInt32(0));
                //texto = string.Format("{2}: Prestamo #{3}# {0} {1}", nombre, modelo, entero, idprestamo);
            }

            texto = string.Format("{2}: {0} {1}", nombre, modelo, entero);


            e.GetComponentInChildren<TextMeshProUGUI>().text = texto;
            e.GetComponent<Image>().color = Color.clear;
            articulos.Add(new Articulo(nombre, modelo));
            e.GetComponent<Button>().onClick.AddListener(delegate { setValor(texto); });
            Elementos.Add(e);
            entero++;
        }
    }

    void setValor(string texto)
    {
        index = int.Parse(texto.Split(':')[0]);
        this.valor = articulos[index].idinventario;
        miImagen.GetComponent<RawImage>().texture = new Texture2D(0, 0, TextureFormat.RGBA32, false);
        foreach (GameObject e in Elementos)
        {
            e.GetComponent<Image>().color = Color.clear;
        }
        Elementos[index].GetComponent<Image>().color = Color.gray;
        if (articulos[index].imagen != null)
        {
            if (articulos[index].imagen.Length < 100)
            {
                return;
            }
            byte[] Bytes = System.Convert.FromBase64String(articulos[index].imagen);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(Bytes);
            miImagen.GetComponent<RawImage>().texture = (texture as Texture);
        }
    }

    public void setWhere(string condicion)
    {
        this.WHERE = condicion;
        RellenarListaDesdeQuery();
    }

    public int getArticuloIdInventario()
    {
        return this.valor;
    }

    public int getIdPrestamo()
    {
        return this.idprestamo[index];
    }
}

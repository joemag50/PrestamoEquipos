﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using System.Data;

public class ManagerLogin : MonoBehaviour
{
	public GameObject CLogin, CPrincipal, inpt_usuario, inpt_pass, LabelUsuarioSesion;

    public static Usuario usuEntrante;


    // Use this for initialization
    void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void Login()
	{
        string usua = inpt_usuario.GetComponent<TMP_InputField>().text;
        string pass = inpt_pass.GetComponent<TMP_InputField>().text;
        usuEntrante = new Usuario(usua, pass);

        if (!usuEntrante.Existe(true))
        {
            Utilerias.ShowPopUp("Usuario: No Existe");
            return;
        }

        if (!usuEntrante.PassCorrecta(pass))
        {
            Utilerias.ShowPopUp("Contraseña: Equivocada");
            return;
        }

        Utilerias.ShowPopUp("Bienvenido: "+usuEntrante.usu_nom);
        LabelUsuarioSesion.GetComponent<TextMeshProUGUI>().text = "Usuario: " + usuEntrante.usu_nom;
        ManagerLogin.log_insert("LOGIN");
        //Si sale todo cool, nos vamos de aqui y vamos al principal
        CLogin.SetActive(false);
		CPrincipal.SetActive(true);
	}

    public void Salir()
    {
        Application.Quit();
    }

    public static void log_insert(string accion)
    {
        BaseDatos.SqlCommand(string.Format(" INSERT INTO bitacora (bit_descrip, bit_idusuario, bit_fecha) " +
                                           " VALUES ('{0}','{1}','{2}')", accion, usuEntrante.idusuario, Prestamo.TiempoDeHoy()));
    }
}

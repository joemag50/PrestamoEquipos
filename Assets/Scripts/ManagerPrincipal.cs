﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ManagerPrincipal : MonoBehaviour
{
	public GameObject CLogin, CPrincipal, CNuevoPrestamo, CRetorno, CAdministracion, LabelUsuarioSesion;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void ButtonNuevoPrestamo()
	{
		CPrincipal.SetActive(false);
		CNuevoPrestamo.SetActive(true);
	}

	public void ButtonRetorno()
	{
		CPrincipal.SetActive(false);
        CRetorno.SetActive(true);

    }

	public void ButtonAdministracion()
	{
		CPrincipal.SetActive(false);
        CAdministracion.SetActive(true);
	}

    public void CerrarSesion()
    {
        CPrincipal.SetActive(false);
        CLogin.SetActive(true);
        ManagerLogin.log_insert("LOGOUT");
        LabelUsuarioSesion.GetComponent<TextMeshProUGUI>().text = "";
    }
}

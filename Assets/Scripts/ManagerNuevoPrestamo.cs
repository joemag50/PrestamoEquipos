﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ManagerNuevoPrestamo : MonoBehaviour
{
	public GameObject CPrincipal, CNuevoPrestamo;

	[Header("Alumno")]
	public GameObject[] inptAlumno;

    [Header("Tabla pedorra")]
    public GameObject Tablapedorra;

	// Use this for initialization
	void Start ()
	{
		BloquearInptAlumno(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
        
	}

    public void GuardarPrestamo()
    {
        Alumno a = GuardarAlumno();
        if (a == null)
        {
            return;
        }

        Articulo arti = new Articulo(Tablapedorra.GetComponent<RellenarLista>().getArticuloIdInventario());
        if (!arti.Existe())
        {
            Utilerias.ShowPopUp("No existe el articulo, favor de seleccionarlo");
            return;
        }

        Prestamo p = new Prestamo(a.dir_matricula, arti.idinventario, Prestamo.TiempoDeHoy(), ManagerLogin.usuEntrante.idusuario);
        if (p.ArticuloYaPrestado())
        {
            Utilerias.ShowPopUp("El articulo aun no ha sido retornado");
            return;
        }

        p.sqlInsert();

        //Impresion del ticket?


        Utilerias.ShowPopUp("Prestamo Generado Correctamente");
        ManagerLogin.log_insert("Prestamo Generado Correctamente "+a.dir_matricula+" "+arti.nombre+" "+arti.modelo);
        RegresarPrincipal();
        return;
    }

	public void RellenarInptAlumno()
	{
		string matricula = inptAlumno[1].GetComponent<TMP_InputField>().text;
		Alumno a = new Alumno(matricula);

        if (!a.Existe() && !inptAlumno[0].GetComponent<Toggle>().isOn)
        {
            Utilerias.ShowPopUp("El alumno no existe, favor de marcar la casilla de Nuevo, e ingresar al info correspondiente");
            return;
        }

        inptAlumno[2].GetComponent<TMP_InputField>().text = a.dir_nom_compl;
        inptAlumno[3].GetComponent<TMP_InputField>().text = a.dir_celular;
        inptAlumno[4].GetComponent<TMP_InputField>().text = a.dir_correo;
    }

	public void BloquearInptAlumno(bool valor)
	{
		foreach(GameObject go in inptAlumno)
		{
			if (go.GetComponent<Toggle>() == null && go.name != "InputMatricula")
			{
				go.GetComponent<TMP_InputField>().interactable = valor;
			}
		}
	}

	public Alumno GuardarAlumno()
	{
		bool esNuevo     = inptAlumno[0].GetComponent<Toggle>().isOn;
		string matricula = inptAlumno[1].GetComponent<TMP_InputField>().text;
		string nombre    = inptAlumno[2].GetComponent<TMP_InputField>().text;
		string celular   = inptAlumno[3].GetComponent<TMP_InputField>().text;
		string correo    = inptAlumno[4].GetComponent<TMP_InputField>().text;

		Alumno a = new Alumno(matricula, nombre, celular, correo);

        if (a.dir_matricula.Length == 0)
        {
            Utilerias.ShowPopUp("Favor de completar la matricula del alumno");
            return null;
        }

        if (a.dir_nom_compl.Length == 0)
        {
            Utilerias.ShowPopUp("Favor de completar el nombre del alumno");
            return null;
        }

        if (a.dir_celular.Length == 0)
        {
            Utilerias.ShowPopUp("Favor de completar el celular del alumno");
            return null;
        }

        if (a.Existe())
		{
            return a;
		}
        //Hacemos un sql insert y regresamos el Alumno
        a.SqlInsert();
        return a;
	}

	public void RegresarPrincipal()
	{
        foreach (GameObject go in inptAlumno)
        {
            if (go.GetComponent<Toggle>() == null)
            {
                go.GetComponent<TMP_InputField>().text = "";
            }
        }
        CNuevoPrestamo.SetActive(false);
		CPrincipal.SetActive(true);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System;
using System.Windows.Forms;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class Informes : MonoBehaviour
{
    public Camera c;
    byte[] imageBytes;
    RenderTexture rt;

    private string pdfName = "";
    private string path = "";

    public int Informe;
    private string query, tabla, renglon;

    public void GeneratePDF()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        SaveFileDialog dlg = new SaveFileDialog();
        dlg.DefaultExt = ".pdf";
        dlg.InitialDirectory = UnityEngine.Application.dataPath;
        dlg.Filter = "Pdf documents (.pdf)|*.pdf";
        pdfName = "Informe "+tabla+" (" + System.DateTime.Now.ToString("yyy-MM-dd_HH-mm-ss") + ")";
        dlg.FileName = pdfName;

        if (dlg.ShowDialog() == DialogResult.OK)
        {
            path = dlg.FileName;
            //print("Usuario escogio la ruta");
        }
        else if (dlg.ShowDialog() == DialogResult.Cancel || dlg.ShowDialog() == DialogResult.Abort)
        {
            path = "";
            //print("Usuario cerro la ventana");
        }

        if (path != "")
        {
            CreatePDF(pdfName);
            //print("Pdf ha sido guardado");
        }
#endif
    }

    public void CreatePDF(string fileName)
    {
        MemoryStream stream = new MemoryStream();
        Document doc = new Document(PageSize.A4);
        PdfWriter pdfWriter = PdfWriter.GetInstance(doc, stream);

        PdfWriter.GetInstance(doc, new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None));
        BaseFont bfHelv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
        iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(bfHelv, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
        iTextSharp.text.Font fontBold = new iTextSharp.text.Font(bfHelv, 10, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

        doc.Open();
        doc.NewPage();

        PdfPTable mainTable = new PdfPTable(1); //La tabla del documento
        mainTable.HorizontalAlignment = Element.ALIGN_CENTER;

        PdfPCell tmpCell = new PdfPCell(); //Celda para el titulo
        tmpCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
        tmpCell.BorderWidth = 0;
        tmpCell.AddElement(new Phrase("Informe: "+tabla, fontBold));
        mainTable.AddCell(tmpCell);

        switch (Informe)
        {
            case 0:
                renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}     \t     {4}     \t     {5}",
                    "IdPrestamo", "Matricula", "Inventario", "Fecha Salida", "Fecha Entrada", "Usuario");
                break;
            case 1:
                renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                    "IdUsuario", "Nombre", "Celular", "Correo");
                break;
            case 2:
                renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}     \t     {4}",
                    "IdInventario", "Nombre", "Modelo", "Tipo", "Descripcion");
                break;
            case 3:
                renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                    "IdBitacora", "Descripción", "IdUsuario", "Fecha");
                break;
            case 4:
                renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                    "Matricula", "Nombre", "Celular", "Correo");
                break;
        }

        PdfPCell columnas = new PdfPCell(); //Celda para el titulo
        columnas.Border = iTextSharp.text.Rectangle.NO_BORDER;
        columnas.BorderWidth = 0;
        columnas.Colspan = 2;
        columnas.AddElement(new Phrase(renglon, fontNormal));
        mainTable.AddCell(columnas);

        IDataReader reader = BaseDatos.SqlCommand(query);
        while (reader.Read())
        {
            switch (Informe)
            {
                case 0:
                    renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}     \t     {4}     \t     {5}",
                        reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5));
                    break;
                case 1:
                    renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                        reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
                    break;
                case 2:
                    renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}     \t     {4}",
                        reader.GetInt32(0), reader.GetString(1), reader.GetString(2), tipo_inventario(reader.GetInt32(3)), reader.GetString(4));
                    break;
                case 3:
                    renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                        reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
                    break;
                case 4:
                    renglon = string.Format("{0}     \t     {1}     \t     {2}     \t     {3}",
                         reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3));
                    break;
            }

            PdfPCell renglonx = new PdfPCell(); //Celda para el titulo
            renglonx.Border = iTextSharp.text.Rectangle.NO_BORDER;
            renglonx.BorderWidth = 0;
            renglonx.Colspan = 2;
            renglonx.AddElement(new Phrase(renglon, fontNormal));
            mainTable.AddCell(renglonx);
        }

        PdfPCell tmpCell3 = new PdfPCell(); //Celda para el titulo
        tmpCell3.Border = iTextSharp.text.Rectangle.NO_BORDER;
        tmpCell3.BorderWidth = 0;
        tmpCell3.AddElement(new Phrase("FIN", fontNormal));
        mainTable.AddCell(tmpCell3);

        doc.Add(mainTable);
        doc.Close();

        pdfWriter.Close();
        stream.Close();

        ManagerLogin.log_insert("Impresion de Informe: "+tabla);
        Utilerias.ShowPopUp("Informe impreso en ruta: "+path);
        return;
    }


    public void GenerateTicket(string Params)
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        SaveFileDialog dlg = new SaveFileDialog();
        dlg.DefaultExt = ".pdf";
        dlg.InitialDirectory = UnityEngine.Application.dataPath;
        dlg.Filter = "Pdf documents (.pdf)|*.pdf";
        pdfName = "Informe " + tabla + " (" + System.DateTime.Now.ToString("yyy-MM-dd_HH-mm-ss") + ")";
        dlg.FileName = pdfName;

        if (dlg.ShowDialog() == DialogResult.OK)
        {
            path = dlg.FileName;
            //print("Usuario escogio la ruta");
        }
        else if (dlg.ShowDialog() == DialogResult.Cancel || dlg.ShowDialog() == DialogResult.Abort)
        {
            path = "";
            //print("Usuario cerro la ventana");
        }

        if (path != "")
        {
            CreateTicket(Params);
            //print("Pdf ha sido guardado");
        }
#endif
    }

    public void CreateTicket(string Params)
    {
        MemoryStream stream = new MemoryStream();
        Document doc = new Document(PageSize.A4);
        PdfWriter pdfWriter = PdfWriter.GetInstance(doc, stream);

        PdfWriter.GetInstance(doc, new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None));
        BaseFont bfHelv = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
        iTextSharp.text.Font fontNormal = new iTextSharp.text.Font(bfHelv, 8, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
        iTextSharp.text.Font fontBold = new iTextSharp.text.Font(bfHelv, 10, iTextSharp.text.Font.BOLD, iTextSharp.text.BaseColor.BLACK);

        doc.Open();
        doc.NewPage();

        PdfPTable mainTable = new PdfPTable(1); //La tabla del documento
        mainTable.HorizontalAlignment = Element.ALIGN_CENTER;

        PdfPCell tmpCell = new PdfPCell(); //Celda para el titulo
        tmpCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
        tmpCell.BorderWidth = 0;
        tmpCell.AddElement(new Phrase("Ticket: " + tabla, fontBold));
        mainTable.AddCell(tmpCell);

        PdfPCell tmpCell3 = new PdfPCell(); //Celda para el titulo
        tmpCell3.Border = iTextSharp.text.Rectangle.NO_BORDER;
        tmpCell3.BorderWidth = 0;
        tmpCell3.AddElement(new Phrase("FIN", fontNormal));
        mainTable.AddCell(tmpCell3);

        doc.Add(mainTable);
        doc.Close();

        pdfWriter.Close();
        stream.Close();

        ManagerLogin.log_insert("Impresion de Informe: " + tabla);
        Utilerias.ShowPopUp("Informe impreso en ruta: " + path);
        return;
    }

    public void SetInforme(int informe)
    {
        this.Informe = informe;
        switch (informe)
        {
            case 0:
                query = "SELECT idprestamo, dir_matricula, inv_nombre || ' ' || inv_modelo, fecha_hora_salida, fecha_hora_entrada, idusuario " +
                    " FROM prestamos LEFT JOIN inventario USING(idinv) ORDER BY idprestamo DESC";
                tabla = "Prestamos";
                break;
            case 1:
                query = "SELECT idusuario, usu_nom, usu_celular, usu_correo FROM usuarios ORDER BY idusuario";
                tabla = "Usuarios";
                break;
            case 2:
                query = "SELECT idinv, inv_nombre, inv_modelo, inv_tipo, inv_descripcion  FROM inventario ORDER BY inv_tipo, inv_nombre, inv_modelo";
                tabla = "Inventario";
                break;
            case 3:
                query = "SELECT idbitacora, bit_descrip, bit_idusuario, bit_fecha FROM bitacora";
                tabla = "Bitacora";
                break;
            case 4:
                query = "SELECT dir_matricula, dir_nom_compl, dir_celular, dir_correo FROM directorio ORDER BY dir_matricula";
                tabla = "Directorio";
                break;
        }
    }
	// Use this for initialization
	void Start ()
    {
        SetInforme(0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    string tipo_inventario(int tipo)
    {
        string stipo = "";
        switch (tipo)
        {
            case 0:
                stipo = "Audio";
                break;
            case 1:
                stipo = "Cable";
                break;
            case 2:
                stipo = "Equipo";
                break;
            case 3:
                stipo = "Video";
                break;
        }
        return stipo;
    }
}

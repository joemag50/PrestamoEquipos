﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class Usuario
{
    public string idusuario;
    public string usu_nom;
    public string usu_pass;
    public string usu_celular;
    public string usu_correo;

    public Usuario(string idusuario)
    {
        this.idusuario = idusuario;
        this.usu_pass = "";
        this.Existe(true);
    }
    //Este lo voy a poner para buscarlo y traer la info de base de datos
    public Usuario(string idusuario, string pass)
    {
        this.idusuario = idusuario;
        this.usu_pass = pass;
        this.Existe(true);
    }

    //Este constructor va a ser para dar de alta nuevos
    public Usuario(string idusuario, string nom, string pass, string cel, string correo)
    {
        this.idusuario = idusuario;
        this.usu_nom = nom;
        this.usu_pass = pass;
        this.usu_celular = cel;
        this.usu_correo = correo;
    }

    public bool Existe(bool reemplazar)
    {
        //Buscamos si existe
        IDataReader reader = BaseDatos.SqlCommand(string.Format("SELECT * FROM usuarios WHERE idusuario = '{0}'", this.idusuario));
        bool existe = false;
        if (reader.Read())
        {
            existe = true;
            if (reemplazar)
            {
                this.usu_nom = reader.GetString(1);
                this.usu_pass = reader.GetString(2);
                this.usu_celular = Utilerias.coalesce(reader.GetString(3));
                this.usu_correo = Utilerias.coalesce(reader.GetString(4));
            }
        }

        return existe;
    }

    public bool PassCorrecta(string passEscrita)
    {
        string passReal = "";
        IDataReader reader = BaseDatos.SqlCommand(string.Format("SELECT * FROM usuarios WHERE idusuario = '{0}'", idusuario));
        if (reader.Read())
        {
            passReal = reader.GetString(2);
        }

        return object.Equals(passEscrita, passReal);
    }

    public void SqlInsert()
    {
        BaseDatos.SqlCommand(string.Format(" INSERT INTO usuarios" +
                                           " VALUES ('{0}','{1}','{2}','{3}','{4}')",
                                           this.idusuario, this.usu_nom, this.usu_pass,
                                           this.usu_celular, this.usu_correo));
    }

    public void SqlUpdate()
    {
        BaseDatos.SqlCommand(string.Format(" UPDATE usuarios " +
                                           " SET (usu_nom,usu_celular,usu_correo) = ('{1}','{2}','{3}') " +
                                           " WHERE idusuario = '{0}'",
                                           this.idusuario, this.usu_nom,
                                           this.usu_celular, this.usu_correo));
        if (this.usu_pass != "")
        {
            BaseDatos.SqlCommand(string.Format(" UPDATE usuarios " +
                                              " SET (usu_pass) = ('{1}') " +
                                              " WHERE idusuario = '{0}'",
                                              this.idusuario, this.usu_pass));
        }
    }

    public void SqlDelete()
    {
        BaseDatos.SqlCommand(string.Format(" DELETE FROM usuarios WHERE idusuario = '{0}'",this.idusuario));
    }
}

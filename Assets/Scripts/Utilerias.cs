﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utilerias : MonoBehaviour
{
    public static void ShowPopUp(string text)
    {
        GameObject popup = (GameObject)Resources.Load("popUpCanvas");
        GameObject p = Instantiate(popup, Vector3.zero, Quaternion.identity);
        p.GetComponent<popUpCanvas>().ShowMessage(text);
    }

    public static string coalesce(string texto)
    {
        if (!object.Equals(texto, "Null"))
        {
            return texto;
        }
        return "";
    }

    public static string TextureToB64(GameObject t)
    {
        if (t.GetComponent<RawImage>() == null)
        {
            return "";
        }

        if (t.GetComponent<RawImage>().texture == null)
        {
            return "";
        }

        Texture2D image2D = (t.GetComponent<RawImage>().texture as Texture2D);
        string b64 = System.Convert.ToBase64String(image2D.EncodeToPNG());
        return b64;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerAdministracion : MonoBehaviour
{
    public GameObject CPrincipal, CAdministracion, CUsuarios, CInventario, CInformes;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void NuevoUsuario()
    {
        CAdministracion.SetActive(false);
        CUsuarios.SetActive(true);
    }

    public void NuevoArticulo()
    {
        CAdministracion.SetActive(false);
        CInventario.SetActive(true);
    }

    public void Informes()
    {
        CAdministracion.SetActive(false);
        CInformes.SetActive(true);
    }

    public void Regresar()
    {
        CAdministracion.SetActive(false);
        CPrincipal.SetActive(true);
    }
}

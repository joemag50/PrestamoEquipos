﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class Alumno
{
    public string dir_matricula;
    public string dir_nom_compl;
    public string dir_celular;
    public string dir_correo;

    public Alumno(string matricula)
    {
        this.dir_matricula = matricula;
        RellenarDesdeMatricula();
    }

    public Alumno(string matricula, string nombre, string celular, string correo)
    {
        this.dir_matricula = matricula;
        this.dir_nom_compl = nombre;
        this.dir_celular = celular;
        this.dir_correo = correo;
    }

    public void RellenarDesdeMatricula()
    {
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM directorio " +
                                                                " WHERE dir_matricula = '{0}' ",this.dir_matricula));
        if (reader.Read())
        {
            this.dir_matricula = reader.GetString(0);
            this.dir_nom_compl = reader.GetString(1);
            this.dir_celular   = reader.GetString(2);
            this.dir_correo    = reader.GetString(3);
        }
    }

    public bool Existe()
    {
        bool existe = false;
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM directorio " +
                                                                " WHERE dir_matricula = '{0}' ",this.dir_matricula));
        if (reader.Read())
        {
            existe = true;
        }
        return existe;
    }

    public void SqlInsert()
    {
        BaseDatos.SqlCommand(string.Format(" INSERT INTO directorio (dir_matricula, dir_nom_compl, dir_celular, dir_correo) " +
                                           " VALUES ('{0}','{1}','{2}','{3}')",
                                           this.dir_matricula, this.dir_nom_compl, this.dir_celular, this.dir_correo));
    }

    public void SqlUpdate()
    {
        BaseDatos.SqlCommand(string.Format(" UPDATE directorio SET (dir_nom_compl, dir_celular, dir_correo) = " +
                                           " ('{1}','{2}','{3}') WHERE dir_matricula = '{0}' ",
                                           this.dir_matricula, this.dir_nom_compl, this.dir_celular, this.dir_correo));
    }

    public void SqlDelete()
    {
        BaseDatos.SqlCommand(string.Format(" DELETE FROM directorio WHERE dir_matricula = '{0}' ", this.dir_matricula));
    }
}

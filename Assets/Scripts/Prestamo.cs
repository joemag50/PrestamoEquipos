﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;


public class Prestamo
{
    public int idprestamo;
    public string dir_matricula;
    public int idinv;
    public string fecha_hora_salida;
    public string fecha_hora_entrada;
    public string idusuario;

    public Prestamo(int idprestamo)
    {
        this.idprestamo = idprestamo;
    }

    public Prestamo(string dir_matricula, int idinv, string fecha_hora_salida, string idusuario)
    {
        this.idprestamo = 0;
        this.dir_matricula = dir_matricula;
        this.idinv = idinv;
        this.fecha_hora_salida = fecha_hora_salida;
        this.fecha_hora_entrada = "";
        this.idusuario = idusuario;
    }

    public Prestamo(string dir_matricula, int idinv, string fecha_hora_salida, string fecha_hora_entrada, string idusuario)
    {
        this.idprestamo         = 0;
        this.dir_matricula      = dir_matricula;
        this.idinv              = idinv;
        this.fecha_hora_salida  = fecha_hora_salida;
        this.fecha_hora_entrada = fecha_hora_entrada;
        this.idusuario = idusuario;
    }

    public bool ArticuloYaPrestado()
    {
        bool existe = false;
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM prestamos WHERE idinv = {0} AND (fecha_hora_entrada IS NULL OR fecha_hora_entrada = '' )", this.idinv));
        if (reader.Read())
        {
            existe = true;
        }
        return existe;
    }

    public static string TiempoDeHoy()
    {
        return DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
    }

    public void sqlInsert()
    {
        BaseDatos.SqlCommand(string.Format(" INSERT INTO prestamos (dir_matricula, idinv, fecha_hora_salida, fecha_hora_entrada, idusuario) " +
                                           " VALUES ('{0}', {1},'{2}','{3}', '{4}')",
                                   this.dir_matricula, this.idinv, this.fecha_hora_salida, this.fecha_hora_entrada, this.idusuario));
    }

    public void sqlUpdate()
    {
        BaseDatos.SqlCommand(string.Format(" UPDATE prestamos SET fecha_hora_entrada = '{0}' WHERE idprestamo = {1}", TiempoDeHoy(), this.idprestamo));
    }
}

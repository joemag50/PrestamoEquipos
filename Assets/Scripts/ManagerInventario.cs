﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using TMPro;

public class ManagerInventario : MonoBehaviour
{
    public GameObject CInventario, CAdministracion, CListado, CNuevo, CEditar, CBorrar;

    [Header("Nuevo Articulo")]
    public GameObject[] NuevoArticulo;

    [Header("Editar Articulo")]
    public GameObject[] EditarArticulo;

    [Header("Lista de articulos")]
    public GameObject tablapedorra;
    public GameObject tablapedorr2;

    public GameObject originalRawImage;
    // Use this for initialization
    void Start ()
    {
        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    public void GuardarArticulo()
    {
        string nombre = NuevoArticulo[0].GetComponent<TMP_InputField>().text; //Nombre
        string modelo = NuevoArticulo[1].GetComponent<TMP_InputField>().text; //Modelo
        string descri = NuevoArticulo[2].GetComponent<TMP_InputField>().text; //Descripcion
        int    tipo   = NuevoArticulo[3].GetComponent<TMP_Dropdown>().value;   //Tipo
        string imagen = Utilerias.TextureToB64(NuevoArticulo[4]); //imagen

        if (nombre.Trim() == "")
        {
            Utilerias.ShowPopUp("Favor de completar el nombre");
            return;
        }

        if (modelo.Trim() == "")
        {
            Utilerias.ShowPopUp("Favor de completar el modelo");
            return;
        }

        Articulo arti = new Articulo(nombre, modelo, descri, tipo, imagen);

        if (arti.Existe(false))
        {
            Utilerias.ShowPopUp("Ya existe un articulo con el mismo nombre y modelo");
            return;
        }

        arti.SqlInsert();
        Utilerias.ShowPopUp("Articulo agregado correctamente");

        CancelarNuevoArticulo();

        ManagerLogin.log_insert("Articulo agregado correctamente " + arti.nombre + " " + arti.modelo);

        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        return;
    }

    public void RellenarEditarArticulo()
    {
        string nombre = EditarArticulo[0].GetComponent<TMP_InputField>().text; //Nombre
        string modelo = EditarArticulo[1].GetComponent<TMP_InputField>().text; //Modelo

        Articulo arti = new Articulo(nombre, modelo);

        if (!arti.Existe(true))
        {
            Utilerias.ShowPopUp("No existe el articulo");
            return;
        }

        EditarArticulo[2].GetComponent<TMP_InputField>().text = arti.descripcion; //Descripcion
        EditarArticulo[3].GetComponent<TMP_Dropdown>().value = arti.tipo;   //Tipo
        if (arti.imagen != null)
        {
            byte[] Bytes = System.Convert.FromBase64String(arti.imagen);
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(Bytes);
            EditarArticulo[4].GetComponent<RawImage>().texture = (texture as Texture);
        }

        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
    }

    public void GuardarEditarArticulo()
    {
        string nombre = EditarArticulo[0].GetComponent<TMP_InputField>().text; //Nombre
        string modelo = EditarArticulo[1].GetComponent<TMP_InputField>().text; //Modelo
        string descri = EditarArticulo[2].GetComponent<TMP_InputField>().text; //Descripcion
        int    tipo   = EditarArticulo[3].GetComponent<TMP_Dropdown>().value;   //Tipo
        string imagen = Utilerias.TextureToB64(EditarArticulo[4]);//imagen

        Articulo arti = new Articulo(nombre, modelo, descri, tipo, imagen);

        if (!arti.Existe(false))
        {
            Utilerias.ShowPopUp("No existe el articulo");
            return;
        }

        arti.SqlUpdate();
        Utilerias.ShowPopUp("Articulo editado correctamente");

        CancelarEditarArticulo();

        ManagerLogin.log_insert("Articulo editado correctamente " + arti.nombre + " " + arti.modelo);

        return;
    }

    public void BorrarArticulo()
    {
        int idinventario = tablapedorra.GetComponent<RellenarLista>().valor;
        Articulo arti = new Articulo(idinventario);
        if (!arti.Existe())
        {
            Utilerias.ShowPopUp("El articulo no exite");
            return;
        }
        arti.SqlDelete();
        Utilerias.ShowPopUp("Articulo borrado correctamente");

        ManagerLogin.log_insert("Articulo borrado correctamente " + arti.nombre + " " + arti.modelo);

        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
    }

    public void CancelarEditarArticulo()
    {
        foreach (GameObject go in EditarArticulo)
        {
            if (go.GetComponent<TMP_InputField>() != null)
            {
                go.GetComponent<TMP_InputField>().text = "";
            }

            if (go.GetComponent<TMP_Dropdown>() != null)
            {
                go.GetComponent<TMP_Dropdown>().value = 0;
            }

            if (go.GetComponent<RawImage>() != null)
            {
                go.GetComponent<RawImage>().texture = originalRawImage.GetComponent<RawImage>().texture;
            }
        }
    }
    public void CancelarNuevoArticulo()
    {
        foreach (GameObject go in NuevoArticulo)
        {
            if (go.GetComponent<TMP_InputField>() != null)
            {
                go.GetComponent<TMP_InputField>().text = "";
            }

            if (go.GetComponent<TMP_Dropdown>() != null)
            {
                go.GetComponent<TMP_Dropdown>().value = 0;
            }

            if (go.GetComponent<RawImage>() != null)
            {
                go.GetComponent<RawImage>().texture = originalRawImage.GetComponent<RawImage>().texture;
            }
        }
    }
    public void GUIListado()
    {
        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        CListado.SetActive(true);
        CNuevo.SetActive(false);
        CEditar.SetActive(false);
        CBorrar.SetActive(false);
    }

    public void GUINuevo()
    {
        CancelarNuevoArticulo();
        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        CListado.SetActive(false);
        CNuevo.SetActive(true);
        CEditar.SetActive(false);
        CBorrar.SetActive(false);
    }

    public void GUIEditar()
    {
        CancelarEditarArticulo();
        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        CListado.SetActive(false);
        CNuevo.SetActive(false);
        CEditar.SetActive(true);
        CBorrar.SetActive(false);
    }

    public void GUIBorrar()
    {
        tablapedorra.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        tablapedorr2.GetComponent<RellenarLista>().RellenarListaDesdeQuery();
        CListado.SetActive(false);
        CNuevo.SetActive(false);
        CEditar.SetActive(false);
        CBorrar.SetActive(true);
    }

    public void Regresar()
    {
        CancelarNuevoArticulo();
        CInventario.SetActive(false);
        CAdministracion.SetActive(true);
    }
}

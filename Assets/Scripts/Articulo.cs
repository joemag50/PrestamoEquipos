﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class Articulo
{
    public int idinventario;
    public string nombre;
    public string modelo;
    public string descripcion;
    public int tipo;
    public string imagen;

    public Articulo(int idinventario)
    {
        this.idinventario = idinventario;
        RellenarDesdeId();
    }

    public Articulo (string nombre, string modelo)
    {
        this.nombre = nombre;
        this.modelo = modelo;
        Existe(true);
    }

    public Articulo(string nombre, string modelo, string descripcion, int tipo, string imagen)
    {
        this.idinventario = 0;
        this.nombre = nombre;
        this.modelo = modelo;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.imagen = imagen;
    }

    public void RellenarDesdeId()
    {
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM inventario " +
                                                        " WHERE idinv = {0}",
                                                        this.idinventario));
        if (reader.Read())
        {
            //Este siempre lo voy a entregar, bc no hay manera de saberlo afuera
            this.nombre = reader.GetString(1);
            this.modelo = reader.GetString(2);
            this.descripcion = reader.GetString(3);
            this.tipo = reader.GetInt32(4);
            this.imagen = reader.GetString(5);
        }
    }

    public bool Existe()
    {
        bool existe = false;
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM inventario " +
                                                        " WHERE idinv = {0}",
                                                        this.idinventario));
        if (reader.Read())
        {
            existe = true;
        }
        return existe;
    }
    public bool Existe(bool reemplazar)
    {
        bool existe = false;
        IDataReader reader = BaseDatos.SqlCommand(string.Format(" SELECT * FROM inventario " +
                                                                " WHERE inv_nombre = '{0}' AND inv_modelo = '{1}' ",
                                                                this.nombre, this.modelo));
        if (reader.Read())
        {
            existe = true;
            //Este siempre lo voy a entregar, bc no hay manera de saberlo afuera
            this.idinventario = reader.GetInt32(0);
            if (reemplazar)
            {
                this.descripcion = reader.GetString(3);
                this.tipo = reader.GetInt32(4);
                this.imagen = reader.GetString(5);
            }
        }
        return existe;
    }

    public void SqlInsert()
    {
        BaseDatos.SqlCommand(string.Format(" INSERT INTO inventario (inv_nombre, inv_modelo, inv_descripcion, inv_tipo, inv_imagen) " +
                                           " VALUES ('{0}','{1}','{2}',{3},'{4}')",
                                           this.nombre, this.modelo, this.descripcion, this.tipo, this.imagen));
    }

    public void SqlUpdate()
    {
        BaseDatos.SqlCommand(string.Format(" UPDATE inventario SET (inv_nombre, inv_modelo, inv_descripcion, inv_tipo, inv_imagen) = " +
                                           " ('{0}','{1}','{2}',{3},'{4}') WHERE idinv = {5} ",
                                           this.nombre, this.modelo, this.descripcion, this.tipo, this.imagen, this.idinventario));
    }

    public void SqlDelete()
    {
        BaseDatos.SqlCommand(string.Format(" DELETE FROM inventario WHERE idinv = {0} ", this.idinventario));
    }
}
